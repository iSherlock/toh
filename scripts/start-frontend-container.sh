#!/bin/sh
# THIS SCRIPT IS INTENDED TO BE RUN FROM THE auth1 PROJECT DIRECTORY
docker run --rm -it --network my-net --name frontend \
    -v $PWD:/mnt/toh \
    -p 8080:4200 \
    -w /mnt/toh    bkoehler/feathersjs sh
